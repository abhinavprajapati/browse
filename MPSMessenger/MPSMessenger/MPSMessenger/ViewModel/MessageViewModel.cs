﻿using GalaSoft.MvvmLight.Command;
using MPSMessenger.Backends;
using MPSMessenger.Model;
using MPSMessenger.Services;
using MPSMessenger.Views;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MPSMessenger.ViewModel
{
    public class MessageViewModel : VMBase
    {
        private ObservableCollection<JoinedRoom> joinedRoomCollection = new ObservableCollection<JoinedRoom>();
        private ObservableCollection<string> rooms = new ObservableCollection<string>();
        private ObservableCollection<RoomModel> roomName;
        private ObservableCollection<JoinedRoom> roomMemberCollection = new ObservableCollection<JoinedRoom>();
        private ObservableCollection<Chunk> roomMemberModelCollection = new ObservableCollection<Chunk>();
        private ObservableCollection<ChunkDetail> roomMessegeCollection = new ObservableCollection<ChunkDetail>();
        MatrixAPI matrixAPI;
        private string header = string.Empty;
        private string roomID = string.Empty;
        private string messege = string.Empty;
        // Model.MatrixSync
        private Model.MatrixSync matrixSync = new MatrixSync();
        public MatrixSync MatrixSyncData { get => matrixSync; set { matrixSync = value; RaisePropertyChanged("MatrixSyncData"); } }

        private bool indicatorVisiblity = false;
        public bool IndicatorVisiblity { get => indicatorVisiblity; set { indicatorVisiblity = value; RaisePropertyChanged("IndicatorVisiblity"); } }
        public ObservableCollection<JoinedRoom> JoinedRoomCollection { get => joinedRoomCollection; set { joinedRoomCollection = value; RaisePropertyChanged("JoinedRoomCollection"); } }
        public ObservableCollection<string> Rooms { get => rooms; set { rooms = value; RaisePropertyChanged("Rooms"); } }
        public ObservableCollection<ChunkDetail> RoomMessegeCollection { get => roomMessegeCollection; set { roomMessegeCollection = value; RaisePropertyChanged("RoomMessegeCollection"); } }
        public ObservableCollection<JoinedRoom> RoomMemberCollection { get => roomMemberCollection; set { roomMemberCollection = value; RaisePropertyChanged("RoomMemberCollection"); } }
        public ObservableCollection<Chunk> RoomMemberModelCollection { get => roomMemberModelCollection; set { roomMemberModelCollection = value; RaisePropertyChanged("RoomMemberModelCollection"); } }
        public string Header { get => header; set { header = value; RaisePropertyChanged("Header"); } }
        public string RoomID { get => roomID; set { roomID = value; RaisePropertyChanged("RoomID"); } }
        public Dictionary<string, string> RoomIdandName { get; set; }
        public ObservableCollection<RoomModel> RoomNames { get => roomName; set { roomName = value; RaisePropertyChanged("RoomNames"); } }
        public string Messege { get => messege; set { messege = value; RaisePropertyChanged("Messege"); } }


        public ICommand RoomTapCommand { get; set; }
        public ICommand GoBackCommand { get; set; }
        public ICommand OpenParticipentsCommand { get; set; }
        public ICommand SendMessegeCommand { get; set; }

        public MessageViewModel()
        {
            // MatrixAPI.OnReceivedSync += SyncService_OnReceivedSync;

            matrixAPI = new MatrixAPI();
            InitializeCommands();
            ClientSync();
            //GetRooms();
        }

        public async Task ClientSync()
        {
            IndicatorVisiblity = true;
            //Device.StartTimer(TimeSpan.FromSeconds(1000), () =>
            // {

            //     Task.Run(async () =>
            MatrixSyncData = await matrixAPI.ClientSync();
            if (MatrixSyncData != null)
                if (MatrixSyncData.Rooms != null)
                    if (MatrixSyncData.Rooms.Join != null)
                        if (MatrixSyncData.Rooms.Join.Count > 0)
                        {
                            AddRoomToCollection();
                            IndicatorVisiblity = false;
                        }


            // return true;
            //});


        }
        private void AddRoomToCollection()
        {
            //var RoomNameList = new ObservableCollection<RoomModel>();
            if (RoomIdandName != null)
            {
                RoomIdandName.Clear();
                RoomNames.Clear();
            }
            else
            {
                RoomIdandName = new Dictionary<string, string>();
                RoomNames = new ObservableCollection<RoomModel>();
            }
            foreach (var item in MatrixSyncData.Rooms.Join)
            {
                var roomModel = new RoomModel();
                Rooms.Add(item.Key);
                var subItem = item.Value;
                var RoomName = subItem.Timeline.Events.Find(x => x.Type == "m.room.name");
                if (RoomName == null)
                    RoomName = subItem.State.Events.Find(x => x.Type == "m.room.name");
                var messages = subItem.Timeline.Events.FindAll(x => x.Type == "m.room.message");
                if (RoomName != null)
                {
                    var content = RoomName as Model.Events.Room.Name;
                    RoomIdandName.Add(item.Key, content.Content.Name);
                    roomModel.RoomId = item.Key;
                    roomModel.RoomName = content.Content.Name;
                    if (messages != null && messages.Count > 0)
                    {
                        var message = messages[messages.Count - 1] as Model.Events.Room.Message;
                        roomModel.LastMessage = message.Content.Body;
                        roomModel.MessageTime = message.OriginServerTimestamp;
                        var time = roomModel.dateTime;
                    }
                    else
                    {
                        roomModel.LastMessage = string.Empty;
                        roomModel.MessageTime = 0;
                    }
                    RoomNames.Add(roomModel);
                    //if (Rooms.Count != MatrixSyncData.Rooms.Join.Count)
                    //{
                    //    Rooms.Clear();

                    //    foreach (var item in MatrixSyncData.Rooms.Join)
                    //    {
                    //        Rooms.Add(item.Key);
                    //    }
                    //}
                    //RoomNames = RoomNameList;
                }
            }
        }


        //private void SyncService_OnReceivedSync(object sender, MatrixSync e)
        //{
        //}

        private void InitializeCommands()
        {
            RoomTapCommand = new RelayCommand<RoomModel>(RoomTap);
            GoBackCommand = new RelayCommand(GoBack);
            OpenParticipentsCommand = new RelayCommand(OpenParticipents);
            SendMessegeCommand = new RelayCommand(SendMessege);

        }

        async void SendMessege()
        {
            string _messeges = await matrixAPI.SendMesseges(RoomID, Messege);
            var resultDMessege = JsonConvert.DeserializeObject<SendMessegeModel>(_messeges);

            if (!string.IsNullOrEmpty(resultDMessege.event_id))
            {
                RoomMessegeCollection.Add(new ChunkDetail { content = new Content() { body = Messege, }, IsSender = true, event_id = resultDMessege.event_id });
                Messege = string.Empty;
                ClientSync();
            }
        }

        async void OpenParticipents()
        {
            await Application.Current.MainPage.Navigation.PushAsync(new RoomMembersView());
        }

        async void RoomTap(RoomModel roomName)
        {

            var data = MatrixSyncData.Rooms.Join[roomName.RoomId];
            RoomID = roomName.RoomId;
            // var ob = obj as JoinedRoom;
            Header = roomName.RoomName;
            //  var result = await matrixAPI.RoomMembers(ob.JoinedRooms);
            string messeges = await matrixAPI.RoomMesseges(roomName.RoomId);
            var resultDMessege = JsonConvert.DeserializeObject<MessegeResponseModel>(messeges);
            // var resultD = JsonConvert.DeserializeObject<RoomMemberModel>(result);
            List<ChunkDetail> messegeList = new List<ChunkDetail>();


            if (resultDMessege.chunk.Count > 0)
            {
                RoomMemberModelCollection.Clear();
                RoomMemberCollection.Clear();
                RoomMessegeCollection.Clear();
                for (int i = 0; i < resultDMessege.chunk.Count; i++)
                {
                    ChunkDetail chunk = new ChunkDetail();
                    chunk = resultDMessege.chunk[i];
                    messegeList.Add(chunk);
                    //  JoinedRoom room = new JoinedRoom();
                    //  room.JoinedRooms = chunk.content.displayname;
                    // RoomMemberCollection.Add(room);
                }
                messegeList.Reverse();
                for (int i = 0; i < messegeList.Count; i++)
                {
                    if (!string.IsNullOrEmpty(messegeList[i].content.body))
                    {
                        RoomMessegeCollection.Add(messegeList[i]);
                    }
                    //  JoinedRoom room = new JoinedRoom();
                    //  room.JoinedRooms = chunk.content.displayname;
                    // RoomMemberCollection.Add(room);
                }
                await Application.Current.MainPage.Navigation.PushAsync(new ConversationView());

            }

        }

        async void GetRooms()
        {
            IndicatorVisiblity = true;
            try
            {
                string result = await matrixAPI.UserJoinedRooms();
                var resultD = JsonConvert.DeserializeObject<JoinedRoomModel>(result);
                if (resultD.joined_rooms.Count > 0)
                {
                    for (int i = 0; i < resultD.joined_rooms.Count; i++)
                    {
                        JoinedRoom room = new JoinedRoom();
                        room.JoinedRooms = resultD.joined_rooms[i];
                        JoinedRoomCollection.Add(room);
                    }
                }
                IndicatorVisiblity = false;
            }
            catch (Exception ex)
            {
                IndicatorVisiblity = false;

                throw;
            }
        }

        private void GoBack()
        {
            NavigationService.GetNavigationService.GoBack();
        }
    }
}
