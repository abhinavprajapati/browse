﻿using GalaSoft.MvvmLight.Command;
using MPSMessenger.Backends;
using MPSMessenger.Model;
using MPSMessenger.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace MPSMessenger.ViewModel
{
    public class LoginViewModel : VMBase
    {
        MatrixAPI matrixAPI;
        private bool indicatorVisiblity = false;
        private string _userName;
        public string UserName { get => _userName; set { _userName = value; RaisePropertyChanged("UserName"); } }

        private string _userEmailId;
        public string UserEmailId { get => _userEmailId; set { _userEmailId = value; RaisePropertyChanged("UserEmailId"); } }

        private Boolean _userNameOk = false;
        public Boolean UserNameOk { get => _userNameOk; set { _userNameOk = value; RaisePropertyChanged("UserNameOk"); } }

        private string password;
        public string Password { get => password; set { password = value; RaisePropertyChanged("UserName"); } }
        public LoginViewModel()
        {
            InitializeCommands();
            matrixAPI = new MatrixAPI();
        }
        public ICommand LoginCommand { get; set; }
        public ICommand RegisterCommand { get; set; }
        public ICommand NotHaveAnAcoountCommand { get; set; }
        public ICommand CheckUserNameCommand { get; set; }
        public ICommand LogoutCommand { get; set; }
        public bool IndicatorVisiblity { get => indicatorVisiblity; set { indicatorVisiblity = value; RaisePropertyChanged("IndicatorVisiblity"); } }

        private void InitializeCommands()
        {
            IndicatorVisiblity = false;
            LoginCommand = new RelayCommand(UserLogin);
            LogoutCommand = new RelayCommand(UserLogout);

            NotHaveAnAcoountCommand = new RelayCommand(DoNotHaveAnAcoount);
            RegisterCommand = new RelayCommand(UserRegister);
            CheckUserNameCommand = new RelayCommand(CheckUserName);
        }

        async void CheckUserName()
        {
            if (!string.IsNullOrEmpty(UserName))
            {

                IndicatorVisiblity = true;
                DoNotHaveAnAccount notHaveAnaccount = new DoNotHaveAnAccount();
                notHaveAnaccount.username = UserName;
                bool result = await matrixAPI.CheckUserName(notHaveAnaccount);
                if (result)
                {
                    UserNameOk = true;
                }
                else
                {
                    UserNameOk = false;
                }
                IndicatorVisiblity = false;
            }
            else
            {
                //Please enter you username
            }
        }

        async void UserLogout()
        {
           

        }

        async void DoNotHaveAnAcoount()
        {
            IndicatorVisiblity = false;

            await Application.Current.MainPage.Navigation.PushAsync(new RegistrationView());
        }
        async void UserRegister()
        {
            MatrixRegister matrixRegister = new MatrixRegister();
            matrixRegister.BindEmail = false;
            matrixRegister.Password = Password;
            matrixRegister.Username = UserName;
            await matrixAPI.ClientRegister(matrixRegister);
        }

        async void UserLogin()
        {
            try
            {
                //if (!string.IsNullOrEmpty(UserName))
                //{
                //    if (!string.IsNullOrEmpty(Password))
                //    {
                        IndicatorVisiblity = true;
                        LoginModel loginModel = new LoginModel();
                        loginModel.type = "m.login.password";
                        loginModel.password = "Welcome@12345#";
                        loginModel.initial_device_display_name = "";
                        loginModel.identifier.type = "m.id.user";
                        loginModel.identifier.user = "superadmin";
                        LoginResponse result = await matrixAPI.ClientLogin(loginModel);
                        var dd = await matrixAPI.ClientProfile(result.user_id);

                        await Application.Current.MainPage.Navigation.PushAsync(new DashboardView());

                        IndicatorVisiblity = false;
                //    }
                //    else
                //    {
                //        //Please enter you password
                //    }
                //}
                //else
                //{
                //    //Please enter you username
                //}
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
