﻿using GalaSoft.MvvmLight.Command;
using MPSMessenger.Backends;
using MPSMessenger.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace MPSMessenger.ViewModel
{
    public class MoreViewModel : VMBase
    {

        MatrixAPI matrixAPI;
        private string _userName;
        public string UserName { get => _userName; set { _userName = value; RaisePropertyChanged("UserName"); } }
        public MoreViewModel()
        {
            matrixAPI = new MatrixAPI();
            UserName = Application.Current.Properties["UserName"].ToString();
            InitializeCommands();
        }
        private void InitializeCommands()
        {
            //LoginCommand = new RelayCommand(UserLogin);
        }



        private async void UserLogin()
        {
            await Application.Current.MainPage.Navigation.PushAsync(new LoginView());
        }
    }
}
