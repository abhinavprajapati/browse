﻿using CommonServiceLocator;
using GalaSoft.MvvmLight.Ioc;
using System;
using System.Collections.Generic;
using System.Text;

namespace MPSMessenger.ViewModel
{
    public enum NavigatePage
    {
        LoginView = 1,
        DashboardView = 2,
        MessagesView = 3,
        MoreView = 4,
        RegistrationView = 5,
        RoomMembersView = 6,
        UserDetailsView = 7,
        ConversationView = 8,
        CallsView =9,
        SettingsView = 10

    }
    public class ViewModelLocator
    {
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            SimpleIoc.Default.Register<LoginViewModel>();
            SimpleIoc.Default.Register<MoreViewModel>();
            SimpleIoc.Default.Register<MessageViewModel>();
        }

        public LoginViewModel LoginVM
        {
            get { return ServiceLocator.Current.GetInstance<LoginViewModel>(); }
        }
        public MoreViewModel MoreVM
        {
            get { return ServiceLocator.Current.GetInstance<MoreViewModel>(); }
        }

        public MessageViewModel MessegeVM
        {
            get { return ServiceLocator.Current.GetInstance<MessageViewModel>(); }
        }

    }
}
