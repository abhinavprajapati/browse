﻿using GalaSoft.MvvmLight;
using MPSMessenger.Helper;
using MPSMessenger.Interface;
using MPSMessenger.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
namespace MPSMessenger.ViewModel
{
    public class VMBase : ViewModelBase
    {
        public async void RunSafe(Func<Task> delegateFunction)
        {
            try
            {
                await delegateFunction();
            }
            catch (InternetNotAvailableException ex)
            {
                DependencyService.Get<INativeDependencyService>().ShowMessage(ex.Message);
            }
            catch (BusinessException ex)
            {
                DependencyService.Get<INativeDependencyService>().ShowMessage(ex.Message);
            }
            catch (ServiceAPIException ex)
            {
                DependencyService.Get<INativeDependencyService>().ShowMessage(ex.Message);
            }
            catch (Exception ex)
            {
                DependencyService.Get<INativeDependencyService>().ShowMessage(ConstantHelper.ExceptionHandlingConstants.GENERAL_EXCEPTION);
            }
        }
        public async Task<object> RunSafe(Func<Task<object>> delegateFunction)
        {
            object result = null;
            try
            {
                result = await delegateFunction();
            }
            catch (InternetNotAvailableException ex)
            {
                DependencyService.Get<INativeDependencyService>().ShowMessage(ex.Message);
            }
            catch (BusinessException ex)
            {
                DependencyService.Get<INativeDependencyService>().ShowMessage(ex.Message);
            }
            catch (ServiceAPIException ex)
            {
                DependencyService.Get<INativeDependencyService>().ShowMessage(ex.Message);
            }
            catch (Exception ex)
            {
                DependencyService.Get<INativeDependencyService>().ShowMessage(ConstantHelper.ExceptionHandlingConstants.GENERAL_EXCEPTION);
            }
            return result;
        }
    }
}
