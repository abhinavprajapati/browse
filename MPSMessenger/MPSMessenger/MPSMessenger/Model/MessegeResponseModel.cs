﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MPSMessenger
{
    public class MessegeResponseModel
    {
        public List<ChunkDetail> chunk { get; set; }
        public string start { get; set; }
        public string end { get; set; }
    }

    public class Content
    {
       
        public string msgtype { get; set; }
        public string body { get; set; }
    }

    public class Unsigned
    {
        public int age { get; set; }
    }

    public class ChunkDetail
    {
        public ChunkDetail()
        {

        }
        public string type { get; set; }
        public string room_id { get; set; }
        public string sender { get; set; }
        public Content content { get; set; }
        public object origin_server_ts { get; set; }
        public Unsigned unsigned { get; set; }
        public string event_id { get; set; }
        private string _userId = string.Empty;
        public string user_id
        {
            get => _userId; set
            {
                _userId = value;
                string[] id_array = user_id.Split(':');
                SenderName = id_array[0].Split('@')[1];
                if (user_id.ToLower() == Application.Current.Properties["UserId"].ToString().ToLower())
                {
                    IsSender = true;
                    IsSenderName = false;
                }
                else
                {
                    IsSender = false;
                    IsSenderName = true;

                }
            }
        }
        public int age { get; set; }
        public bool IsSender { get; set; }
        public bool IsSenderName { get; set; }
        public string SenderName { get; set; }


       
    }


}
