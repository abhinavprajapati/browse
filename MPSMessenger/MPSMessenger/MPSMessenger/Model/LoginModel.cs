﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace MPSMessenger.Model
{
    public class LoginModel : INotifyPropertyChanged
    {
        public LoginModel()
        {

        }
        public string type { get; set; }
        public string password { get; set; }

        private Identifier _Identifier = new Identifier();

        public event PropertyChangedEventHandler PropertyChanged;
        public Identifier identifier
        {
            get => _Identifier; set
            {
                if (value != this._Identifier)
                {
                    this._Identifier = value;
                    NotifyPropertyChanged("identifier");
                }
            }
        }
        public string initial_device_display_name { get; set; }
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
    public class Identifier
    {
        public string type { get; set; }
        public string user { get; set; }

    }

    public class LoginResponse
    {
        public string user_id { get; set; }
        public string access_token { get; set; }
        public string home_server { get; set; }
        public string device_id { get; set; }
    }

    public class DoNotHaveAnAccount
    {
        public string username { get; set; }

    }
    public class ProfileResponse
    {
        public string displayname { get; set; }
    }

    public class CheckUsernameResponse
    {
        public string available { get; set; }
    }

    

}
