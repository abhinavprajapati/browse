﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace MPSMessenger.Model.Events
{
    [DataContract]
    public class Typing : MatrixEvents
    {
        [DataMember(Name = "content")]
        public TypingContent Content { get; set; }
    }

    [DataContract]
    public class TypingContent
    {
        [DataMember(Name = "user_ids")]
        public string[] UserIDs { get; set; }
    }
}
