﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace MPSMessenger.Model.Events
{
    [DataContract]
    public class Direct : MatrixEvents
    {
        [DataMember(Name = "content")]
        public Dictionary<string, List<string>> Content { get; set; }
    }
}
