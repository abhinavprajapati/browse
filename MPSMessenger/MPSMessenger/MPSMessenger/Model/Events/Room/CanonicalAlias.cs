﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace MPSMessenger.Model.Events.Room
{
    [DataContract]
    public class CanonicalAlias : MatrixEvents
    {
        [DataMember(Name = "content")]
        public AliasContent Content { get; set; }
    }

    [DataContract]
    public class AliasContent
    {
        [DataMember(Name = "alias")]
        public string Alias { get; set; }
    }
}
