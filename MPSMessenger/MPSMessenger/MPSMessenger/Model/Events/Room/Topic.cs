﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace MPSMessenger.Model.Events.Room
{
    [DataContract]
    public class Topic : MatrixEvents
    {
        [DataMember(Name = "content")]
        public TopicContent Content { get; set; }
    }

    [DataContract]
    public class TopicContent
    {
        [DataMember(Name = "topic")]
        public string Topic { get; set; }
    }
}
