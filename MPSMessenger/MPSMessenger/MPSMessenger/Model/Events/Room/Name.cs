﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace MPSMessenger.Model.Events.Room
{
    [DataContract]
    public class Name : MatrixEvents
    {
        [DataMember(Name = "content")]
        public NameContent Content { get; set; }
    }

    [DataContract]
    public class NameContent
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }
    }
}
