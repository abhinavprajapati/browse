﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace MPSMessenger.Model.Events.Room
{
    [DataContract]
    public class Create : MatrixEvents
    {
        [DataMember(Name = "content")]
        public CreateContent Content { get; set; }
    }

    [DataContract]
    public class CreateContent
    {
        [DataMember(Name = "creator")]
        public string Creator { get; set; }
    }
}
