﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace MPSMessenger.Model.Events.Room
{
    [DataContract]
    public class JoinRules : MatrixEvents
    {
        [DataMember(Name = "content")]
        public JoinRulesContent Content { get; set; }
    }

    [DataContract]
    public class JoinRulesContent
    {
        [DataMember(Name = "join_rule")]
        public string JoinRule { get; set; }
    }
}
