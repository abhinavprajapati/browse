﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MPSMessenger.Model
{
    public class MessegeModel
    {
    }
    public class JoinedRoomModel
    {
        public List<string> joined_rooms { get; set; }
    }

    public class JoinedRoom
    {
        public string JoinedRooms { get; set; }
    }

    public class Content
    {
        public string membership { get; set; }
        public string displayname { get; set; }
        public object avatar_url { get; set; }
    }

    public class PrevContent
    {
        public string membership { get; set; }
        public string displayname { get; set; }
        public object avatar_url { get; set; }
    }

    public class Unsigned
    {
        public string replaces_state { get; set; }
        public PrevContent prev_content { get; set; }
        public string prev_sender { get; set; }
        public int age { get; set; }
    }

    public class PrevContent2
    {
        public string membership { get; set; }
        public string displayname { get; set; }
        public object avatar_url { get; set; }
    }

    public class Chunk
    {
        public string type { get; set; }
        public string room_id { get; set; }
        public string sender { get; set; }
        public Content content { get; set; }
        public string state_key { get; set; }
        public object origin_server_ts { get; set; }
        public Unsigned unsigned { get; set; }
        public string event_id { get; set; }
        public string user_id { get; set; }
        public int age { get; set; }
        public string replaces_state { get; set; }
        public PrevContent2 prev_content { get; set; }
    }

    public class RoomMemberModel
    {
        public List<Chunk> chunk { get; set; }
    }



}
