﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MPSMessenger.Model
{
    public class RoomModel
    {
        public string RoomId { get; set; }
        public string RoomName { get; set; }
        public string LastMessage { get; set; }
        public long MessageTime { get; set; }
        public string dateTime { get { return FromUnixTime(MessageTime); } }

        private string FromUnixTime(long unixTime)
        {
            if (unixTime == 0)
                return string.Empty;
            var dt = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(Math.Round(unixTime / 1000d)).ToLocalTime();
            return dt.ToString("HH:mm");
        }
    }
}
