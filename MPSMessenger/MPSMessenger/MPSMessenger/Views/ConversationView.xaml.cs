﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MPSMessenger.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConversationView : ContentPage
    {
        public ConversationView()
        {
            InitializeComponent();
        }

        void TapGestureRecognizer_Tapped(System.Object sender, System.EventArgs e)
        {
            Navigation.PopAsync();
        }
    }
}