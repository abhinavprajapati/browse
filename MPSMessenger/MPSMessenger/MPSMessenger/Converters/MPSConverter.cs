﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace MPSMessenger.Converters
{
    public class IsSenderConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool name = false;
            if (value != null)
            {
                string id = value.ToString();

                string[] id_array = id.Split(':');
                string[] idobj = id_array[0].Split('@');
                string loggedInUser = Application.Current.Properties["UserId"].ToString();
                if(loggedInUser.ToLower()== id)
                {
                    name = true;
                }
            }
            return name;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class RoomNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool name = false;
            if (value != null)
            {
               
            }
            return name;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
