﻿using MPSMessenger.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace MPSMessenger.Helper
{
    public partial class RoomEvents
    {
        public class AccountDataEventArgs : EventArgs
        {
            public MatrixEvents Event;
        }

        public delegate void AccountDataDelegate(object sender, AccountDataEventArgs e);

        public event AccountDataDelegate AccountDataEvent;

        internal void FireAccountDataEvent(MatrixEvents evt) => AccountDataEvent?.Invoke(this, new AccountDataEventArgs() { Event = evt });
    }
}
