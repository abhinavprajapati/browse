﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MPSMessenger.Helper
{
   public class ConstantHelper
    {
        public struct ExceptionHandlingConstants
        {
            public const string NO_INTERNET_CONNECTION = "There is some problem in the internet connectivity.\r\nPlease check and try again.";
            public const string SERVICE_API_ERROR = "A service error occured.\r\nReport if the error persists.";
            public const string GENERAL_EXCEPTION = "An unexpected error has occured. Please contact support team.";
        }

        public struct ApiKeyConstants
        {
            public const string ACCOUNT = "";
            public const string PROFILE = "";
            public const string REGISTER = "";
            

            public const string ROOM_MEMBERSHIP_BAN = "";
            public const string ROOM_MEMBERSHIP_FORGET = "";
            public const string ROOM_MEMBERSHIP_INVITE = "";
            public const string ROOM_MEMBERSHIP_JOIN = "";
            public const string ROOM_MEMBERSHIP_KICK = "";
            public const string ROOM_MEMBERSHIP_LEAVE = "";
            public const string ROOM_MEMBERSHIP_UNBAN = "";
           
        }
        //User data
    }
}
