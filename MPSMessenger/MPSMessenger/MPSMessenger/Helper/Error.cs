﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace MPSMessenger.Helper
{
    [DataContract]
    public class Error
    {
        [DataMember(Name = "errcode")]
        public string ErrorCode { get; set; }
        [DataMember(Name = "error")]
        public string ErrorMsg { get; set; }
    }
}
