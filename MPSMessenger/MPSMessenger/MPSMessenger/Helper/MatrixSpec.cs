﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MPSMessenger.Helper
{
    [AttributeUsage(AttributeTargets.Method)]
    public class MatrixSpec : Attribute
    {
        const string MATRIX_SPEC_URL = "http://matrixsynapse.ddns.net/";
        public readonly string Url;
        public MatrixSpec(string url)
        {
            Url = MATRIX_SPEC_URL + url;
        }

        public override string ToString()
        {
            return Url;
        }
    }
}
