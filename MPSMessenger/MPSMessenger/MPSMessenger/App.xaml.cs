﻿using MPSMessenger.Services;
using MPSMessenger.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MPSMessenger
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            if (!Application.Current.Properties.ContainsKey("Access_Token"))
            {
                LoginView loginView = new LoginView();
                NavigationPage frontPage = new NavigationPage(loginView);
                NavigationService.GetNavigationService.Initialize(frontPage);
                var navigationPage = Current.MainPage as NavigationPage;
                //  MainPage = new NavigationPage();

            }
            else
            {
               // MainPage = new NavigationPage(new DashboardView());
                DashboardView loginView = new DashboardView();
                NavigationPage frontPage = new NavigationPage(loginView);
                NavigationService.GetNavigationService.Initialize(frontPage);
                var navigationPage = Current.MainPage as NavigationPage;
            }
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
