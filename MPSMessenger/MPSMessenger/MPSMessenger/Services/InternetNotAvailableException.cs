﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MPSMessenger.Services
{
    public class InternetNotAvailableException : Exception
    {
        public InternetNotAvailableException(string message) : base(message)
        { }
    }
    public class ServiceAPIException : Exception
    {
        public ServiceAPIException(string message) : base(message)
        { }
    }
    public class BusinessException : Exception
    {
        public BusinessException(string message) : base(message)
        { }
    }
}
