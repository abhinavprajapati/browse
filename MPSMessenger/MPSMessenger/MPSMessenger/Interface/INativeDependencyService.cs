﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MPSMessenger.Interface
{
    public interface INativeDependencyService
    {
        void ShowMessage(string message);
    }
}
