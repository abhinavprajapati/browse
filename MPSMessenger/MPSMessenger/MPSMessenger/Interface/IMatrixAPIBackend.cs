﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MPSMessenger.Interface
{
    public interface IMatrixAPIBackend
    {
        Task<HttpResponseMessage> Get(string path, bool authenticate);
        Task<HttpResponseMessage> Post(string path, bool authenticate, string request);
        Task<HttpResponseMessage> Post(string path, bool authenticate, string request, Dictionary<string, string> headers);
        Task<HttpResponseMessage> Post(string path, bool authenticate, byte[] request, Dictionary<string, string> headers);
        Task<HttpResponseMessage> Put(string path, bool authenticate, string request);

        Task<HttpResponseMessage> Delete(string path, bool authenticate);

        void SetAccessToken(string token);
        string GetPath(string apiPath, bool auth);
    }
}
