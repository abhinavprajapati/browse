﻿using MPSMessenger.Helper;
using MPSMessenger.Interface;
using MPSMessenger.Model;
using MPSMessenger.Views;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MPSMessenger.Backends
{
    public partial class MatrixAPI
    {
        IMatrixAPIBackend _backend = null;
        public static event EventHandler<MatrixSync> OnReceivedSync;

        private string _syncToken = "";
        private RoomEvents _events = null;
        public static MatrixSync sync = new MatrixSync();
        public int SyncTimeout = 10000;
        public bool RunningInitialSync { get; private set; }
        public bool IsConnected
        {
            get; private set;
        }
        public MatrixAPI()
        {
            if (!Uri.IsWellFormedUriString("https://matrixsynapse.ddns.net/", UriKind.Absolute))
                throw new MatrixException("URL is not valid.");
            _events = new RoomEvents();

            _backend = new HttpBackend("https://matrixsynapse.ddns.net/");
            if (Application.Current.Properties.ContainsKey("Access_Token"))
            {
                _syncToken = Application.Current.Properties["Access_Token"].ToString(); ;
                if (string.IsNullOrEmpty(_syncToken))
                    RunningInitialSync = true;
            }

        }

        [MatrixSpec("r0.0.1/client_server.html#post-matrix-client-r0-register")]
        public async Task ClientRegister(MatrixRegister registration)
        {
            var result = await _backend.Post("/_matrix/client/r0/register", false, JsonHelper.Serialize(registration));
            var vdd = result.Content.ReadAsStringAsync().Result;

        }

        [MatrixSpec("r0.0.1/client_server.html#Get-matrix-client-r0-register-available")]
        public async Task<bool> CheckUserName(DoNotHaveAnAccount registration)
        {
            var result = await _backend.Get("/_matrix/client/r0/register/available?username=" + registration.username, false);
            CheckUsernameResponse response = JsonConvert.DeserializeObject<CheckUsernameResponse>(result.Content.ReadAsStringAsync().Result);
            if (response.available.ToLower() == "true")
            {
                return true;
            }
            else
                return false;


        }

        [MatrixSpec("r0.0.1/client_server.html#post-matrix-client-r0-login")]
        public async Task<LoginResponse> ClientLogin(LoginModel model)
        {
            var response = await _backend.Post("/_matrix/client/r0/login", false, JsonHelper.Serialize(model));
            LoginResponse result = JsonConvert.DeserializeObject<LoginResponse>(response.Content.ReadAsStringAsync().Result);
            if (!string.IsNullOrEmpty(result.access_token))
            {
                Application.Current.Properties["Access_Token"] = result.access_token;
                Application.Current.Properties["UserId"] = result.user_id;
                await Application.Current.SavePropertiesAsync();
            }
            return result;
          
        }



        public async Task<string> ClientProfile(string userId)
        {
            var result = await _backend.Get("/_matrix/client/r0/profile/" + userId, true);
            var response = JsonConvert.DeserializeObject<ProfileResponse>(result.Content.ReadAsStringAsync().Result);
            Application.Current.Properties["UserName"] = response.displayname;
            await Application.Current.SavePropertiesAsync();
            return response.displayname;

        }

        public async Task<string> UserJoinedRooms()
        {
            var result = await _backend.Get("/_matrix/client/r0/joined_rooms", true);
            var response = result.Content.ReadAsStringAsync().Result;
            return response;
        }


        public async Task<string> RoomMembers(string roomId)
        {
            var result = await _backend.Get("/_matrix/client/r0/rooms/" + roomId + "/members", true);
            var response = result.Content.ReadAsStringAsync().Result;
            return response;
        }


        public async Task<string> RoomDirectory(string roomId)
        {
            var result = await _backend.Post("/_matrix/client/r0/directory/room/" + roomId + "/read_markers", false, "");
            var response = result.Content.ReadAsStringAsync().Result;
            return response;
        }

        //PUT 
        public async Task<string> SendMesseges(string roomId, string messege)
        {
            SendMessegeModel send = new SendMessegeModel();
            send.msgtype = "m.text";
            send.body = messege;
            var _random = new System.Random();

            var result = await _backend.Put("/_matrix/client/r0/rooms/" + roomId + "/send/m.room.message/" + _random.Next().ToString(), true, JsonHelper.Serialize(send));
            var response = result.Content.ReadAsStringAsync().Result;
            return response;
        }
        public async Task<string> RoomMesseges(string roomId)
        {
            var result = await _backend.Get("/_matrix/client/r0/rooms/" + roomId + "/messages?from=&dir=b&limit=100&filter=", true);
            var response = result.Content.ReadAsStringAsync().Result;
            return response;
        }


        public async Task<MatrixSync> ClientSync(bool connectionFailureTimeout = false, bool fullState = false)
        {
            _syncToken = Application.Current.Properties["Access_Token"].ToString(); ;
            string url = "/_matrix/client/r0/sync?timeout=" + SyncTimeout;
            var tuple = await _backend.Get(url, true);
            var response = tuple.Content.ReadAsStringAsync().Result;
            Model.MatrixSync syncResponse = await ParseClientSync(response);
            if (RunningInitialSync)
            {
                // Fire an event to say sync has been done
                RunningInitialSync = false;
            }
            return syncResponse;
        }
        public static bool CheckNull()
        {
            bool _isnull = false;
            if (OnReceivedSync == null)
            {
                _isnull = true;
            }
            return _isnull;
        }


        private async Task<MatrixSync> ParseClientSync(string resp)
        {
            try
            {
                //using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(resp)))
                {
                    /*DataContractJsonSerializerSettings settings = new DataContractJsonSerializerSettings();
                    settings.UseSimpleDictionaryFormat = true;

                    var ser = new DataContractJsonSerializer(typeof(Responses.MatrixSync), settings);
                    Responses.MatrixSync response = (ser.ReadObject(stream) as Responses.MatrixSync);*/

                    Model.MatrixSync response = Newtonsoft.Json.JsonConvert.DeserializeObject<Model.MatrixSync>(resp);

                    _syncToken = response.NextBatch;

                    foreach (var room in response.Rooms.Join)
                    {
                        // Fire event for room joined
                        _events.FireRoomJoinEvent(room.Key, room.Value);
                    }

                    foreach (var room in response.Rooms.Invite)
                    {
                        // Fire event for invite
                        _events.FireRoomInviteEvent(room.Key, room.Value);
                    }

                    foreach (var room in response.Rooms.Leave)
                    {
                        // Fire event for room leave
                        _events.FireRoomLeaveEvent(room.Key, room.Value);
                    }

                    if (response.Presense != null)
                    {
                        foreach (var evt in response.Presense.Events)
                        {
                            var actualEvent = evt as Model.Events.Presence;
                            bool active = actualEvent.Content.CurrentlyActive;
                        }
                    }

                    if (response.AccountData != null)
                    {
                        foreach (var evt in response.AccountData.Events)
                        {
                            Debug.WriteLine("AccountData Event: " + evt.Type);
                            _events.FireAccountDataEvent(evt);
                        }
                    }
                    // OnReceivedSync.Invoke( null, response);
                    // Do stuff
                    IsConnected = true;
                    return response;
                }
            }
            catch (Exception e)
            {
                throw new MatrixException("Failed to parse ClientSync - " + e.Message);
            }
        }

        private static void OnTweetReceived(MatrixSync syncData)
        {
            OnReceivedSync?.Invoke(null, sync);
        }

    }
    public class SendMessegeModel
    {
        public string msgtype { get; set; }
        public string body { get; set; }
        public string event_id { get; set; }
        

    }
}
