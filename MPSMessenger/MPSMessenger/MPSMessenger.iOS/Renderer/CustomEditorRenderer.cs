﻿using System;
using System.ComponentModel;
using MPSMessenger.CustomControl;
using MPSMessenger.iOS.Renderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomEditor), typeof(CustomEditorRenderer))]

namespace MPSMessenger.iOS.Renderer
{
    public class CustomEditorRenderer : EditorRenderer
    {
        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            try
            {
                if (Control != null)
                {
                    Control.Layer.BorderWidth = 0;
                }

            }
            catch (Exception ex)
            {

            }


        }
    }
}
